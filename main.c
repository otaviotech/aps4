#include <stdio.h>
#include <time.h>
#include <malloc.h>

clock_t getCurrentTime(){
    return clock();
}

double getElapsedTime(clock_t timeBefore, clock_t timeAfter){
    double diffticks = timeAfter - timeBefore;
    double diffms = (diffticks)/(CLOCKS_PER_SEC/1000);
    return diffms;
}

void bubble_sort(long *arr[], int n) {

    long temp;
    int i = 0;
    int j= 0;

    while (i < n) {
        j = 0;
        while (j < i) {
            if (*(arr + j) > *(arr + i)) {
                temp = *(arr +j);
                *(arr + j) = *(arr + i);
                *(arr + i) = temp;
            }
            j++;
        }
        i++;
    }
}

void quick_sort(int *arr[], int first_index, int last_index) {

    int pivotIndex, temp, index_a, index_b;

    if (first_index < last_index) {

        pivotIndex = first_index;
        index_a = first_index;
        index_b = last_index;

        while (index_a < index_b) {
            while (*(arr + index_a) <= *(arr + pivotIndex) && index_a < last_index) {
                index_a++;
            }
            while (*(arr + index_b) > *(arr + pivotIndex)) {
                index_b--;
            }

            if (index_a < index_b) {
                // Swapping operation
                temp = *(arr + index_a);
                *(arr + index_a) = *(arr + index_b);
                *(arr + index_b) = temp;
            }
        }

        temp = *(arr + pivotIndex);
        *(arr + pivotIndex) = *(arr + index_b);
        *(arr + index_b) = temp;

        quick_sort(arr, first_index, index_b - 1);
        quick_sort(arr, index_b + 1, last_index);
    }
}

void insertion_sort(int *arr[], int n) {
    for (int i = 0; i < n; i++) {
        int j = i;
        while (j > 0 && *(arr  + j - 1) > *(arr  + j)) {
            int temp = *(arr  + j);
            *(arr  + j) = *(arr  + j - 1);
            *(arr  + j - 1) = temp;
            j--;
        }
    }
}

double benchmarkBubbleSort(int nItems){

    clock_t timeBefore, timeAfter;


    int i;
    long **arr;

    arr = malloc (sizeof (long *) * nItems);

    i = 0;
    while(i < nItems){
        *(arr + i) = nItems - i;
        i++;
    }

    timeBefore = getCurrentTime();

    bubble_sort(arr, nItems);

    timeAfter = getCurrentTime();

    double diff = getElapsedTime(timeBefore, timeAfter);

    printf("BubbleSort levou %f milisegundos para ordenar %i itens.\n", diff, nItems);

    return diff;

}

double benchmarkQuickSort(int nItems){

    clock_t timeBefore, timeAfter;

    int i;
    long **arr;

    arr = malloc (sizeof (long *) * nItems);

    i = 0;
    while(i < nItems){
        *(arr + i) = nItems - i;
        i++;
    }

    timeBefore = getCurrentTime();

    quick_sort(arr, 0, nItems - 1);

    timeAfter = getCurrentTime();

    double diff = getElapsedTime(timeBefore, timeAfter);

    printf("QuickSort levou %f milisegundos para ordenar %i itens.\n", diff, nItems);

    return diff;

}

double benchmarkInsertionSort(int nItems){

    clock_t timeBefore, timeAfter;


    int i;
    long **arr;

    arr = malloc (sizeof (long *) * nItems);

    i = 0;
    while(i < nItems){
        *(arr + i) = nItems - i;
        i++;
    }

    timeBefore = getCurrentTime();

    insertion_sort(arr, nItems);

    timeAfter = getCurrentTime();

    double diff = getElapsedTime(timeBefore, timeAfter);

    printf("InsertionSort levou %f milisegundos para ordenar %i itens.\n", diff, nItems);
    return diff;

}

void printResults(double *bubbleSortResults[],
                  double *insertionSortResults[],
                  double *quickSortResults[],
                  int bubbleSortSize,
                  int insertionSortSize,
                  int quickSortSize)
{

    double quickSortAvg;

    int i = 0;

    for (i = 0; i < quickSortSize; i++ ){
        printf("%f\n", *quickSortResults);
    }

}

int main() {

    printf("\n============\n");
    printf("Benchmarks:");
    printf("\n============\n");

    double bubbleSortResults[] = {
        benchmarkBubbleSort(10),
        benchmarkBubbleSort(100),
        benchmarkBubbleSort(1000),
        benchmarkBubbleSort(10000),
        benchmarkBubbleSort(100000)
    };

    printf("\n");

    double insertionSortResults[] = {
        benchmarkInsertionSort(10),
        benchmarkInsertionSort(100),
        benchmarkInsertionSort(1000),
        benchmarkInsertionSort(10000),
        benchmarkInsertionSort(100000),
    };

    printf("\n");

    double quickSortResults[] = {
        benchmarkQuickSort(10),
        benchmarkQuickSort(100),
        benchmarkQuickSort(1000),
        benchmarkQuickSort(10000),
        benchmarkQuickSort(100000),
    };

    double bsAvg;

    for(int i = 0; i < (int)sizeof(bubbleSortResults) / (int)sizeof(double); i++){
        bsAvg += bubbleSortResults[i];
    }

    bsAvg = bsAvg / (int)sizeof(bubbleSortResults) / (int)sizeof(double);

    double isAvg;

    for(int i = 0; i < (int)sizeof(insertionSortResults) / (int)sizeof(double); i++){
        isAvg += insertionSortResults[i];
    }

    isAvg = isAvg / (int)sizeof(insertionSortResults) / (int)sizeof(double);

    double qsAvg;

    for(int i = 0; i < (int)sizeof(quickSortResults) / (int)sizeof(double); i++){
        qsAvg += quickSortResults[i];
    }

    qsAvg = qsAvg / (int)sizeof(quickSortResults) / (int)sizeof(double);

    printf("\n============\n");
    printf("Médias:");
    printf("\n============\n");

    printf("BubbleSort:    %f milisegundos. \n", bsAvg);
    printf("InsertionSort: %f milisegundos. \n", isAvg);
    printf("QuickSort:     %f milisegundos. \n", qsAvg);

    printf("\n");

    printf("\n============\n");
    printf("Relatório:");
    printf("\n============\n");


    double x = (qsAvg *100)/bsAvg;
    double y = (qsAvg *100)/isAvg;
    printf("QuickSort é %5.2f%% mais rápido que o BubbleSort.\n", 100 - x);
    printf("QuickSort é %5.2f%% mais rápido que o InsertionSort. \n\n", 100 - y);

    x = (bsAvg *100)/isAvg;
    y = (qsAvg *100)/bsAvg;
    printf("BubleSort é %5.2f%% mais rápido que o InsertionSort.\n", 100 - x);
    printf("BubleSort é %5.2f%% mais lento que o QuickSort.\n\n", 100 - y);

    x = (bsAvg *100)/isAvg;
    y = (qsAvg *100)/isAvg;
    printf("InsertionSort é %5.2f%% mais lento que o BubleSort.\n", 100 - x);
    printf("InsertionSort é %5.2f%% mais lento que o QuickSort.\n", 100 - y);

    return 0;
}

